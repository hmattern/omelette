# OMELETTE: Openly available sMall vEsseL sEgmenTaTion pipelinE

This is a easy-to-use and fast-to-tune vessel segmentation pipeline. 
For vessel enhancement, the filter by  Jerman et al. [1] was reimplemented in python. 
To that end, scikit-image was leveraged.
To segment the vessels from the enhanced images, hysteresis thresholding is used.
To select the required high and low threshold, a 3-class Otus analysis of the signal intensity histogram is used.
Both functions (hysteresis and multi-class Otus) are provided by the scikit-image.
The thresholding can be performed iteratively or non-iteratively.
While the non-iterative method is fast and parameter-free, hence, requires no tuning, the iterative approach is able to segment more smaller vessels.
From the segmented vessels the skeleton is extracted.

For an example and overview please have a look at `Example2D.py` and `Example2Diterative.py`.
Since OMELETTE uses thes same functions for 2D and 3D data, based on these 2D examples a 3D pipeline can be set up easily. 

Conference and challenge submission are included in the folder `data_and_results`.
    
If you use OMELETTE please acknowledge its use by citing the corresponding conference abstract:

>Hendrik Mattern; 
Openly available sMall vEsseL sEgmenTaTion pipelinE (OMELETTE); 
29th Annual Meeting of International Society of Magnetic Resonance in Medicine (ISMRM), May 2021, virtual meeting

Thank you!

## Requirements

Please install:
- scikit-image
- nibabel
- h5py

All remaining required packages (i.e. numpy, matplotlib) will be installed automatically as they are dependencies of the above mentioned packages. A full list of required packages is provided in requirements.txt (generated by $pip freeze > requirements.txt).

## ISMRM2021 
To reproduce the segmentation submitted to the 2021 ISMRM the following datasets need to be downloaded first:
- studyforrest angiograms [3]: http://studyforrest.org/

Compared to the BVM2021 pipeline, a novel iterative thresholding approach was implemented.
The parameter-free, easy-to-tune thresholding is still supported (see `Example2D.py` and all the BVM scripts). 
The novel iterative thresholding is shown in `Example2Diterative.py`. 
All figures and results from sensitivity as well as specificity used in the ISMRM abstract are labeled with `*ISMRM*` in the filename.
 
Please note that the reference segmentations obtained by ilastik [5] are not part of this repository to keep the required memory within a reasonable range (each segmentation is several hunderts of MB).  
The reference segmentations will be made available soon via external data hosting.

## DRIVE Challenge:
Required installation of pillow (former PIL) package for image i/o.
Average Dice score for testing set: 0.7575
Jupyter notebook (used for submission) can be found under `data_and_results\drive_notebook.ipynb`.

## References
[1] Jerman, Tim, et al. "Beyond Frangi: an improved multiscale vesselness filter." Medical Imaging 2015: Image Processing. Vol. 9413. International Society for Optics and Photonics, 2015.

[2] Forstmann, Birte U., et al. "Multi-modal ultra-high resolution structural 7-Tesla MRI data repository." Scientific data 1.1 (2014): 1-8.

[3] Hanke, Michael, et al. "A high-resolution 7-Tesla fMRI dataset from complex natural stimulation with an audio movie." Scientific data 1.1 (2014): 1-18.

[4] Acosta-Cabronero, Julio, et al. "A robust multi-scale approach to quantitative susceptibility mapping." Neuroimage 183 (2018): 7-24.

[5] Sommer, Christoph, et al. "Ilastik: Interactive learning and segmentation toolkit." 2011 IEEE international symposium on biomedical imaging: From nano to macro. IEEE, 2011. 
 
## Resources

The original Matlab implementation of the Jerman filter is provided here:
https://github.com/timjerman/JermanEnhancementFilter

To better understand the python filter implementation, it is recommended to have a look at the following function of scikit-image: 

https://github.com/scikit-image/scikit-image/blob/master/skimage/filters/ridges.py

https://github.com/scikit-image/scikit-image/blob/master/skimage/feature/corner.py

For the interested reader, this homepage explains the rational of the Heassian filtering visually:
https://milania.de/blog/Introduction_to_the_Hessian_feature_detector_for_finding_blobs_in_an_image
