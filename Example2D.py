# EXAMPLE:
# Here we illustrate the capabilities of OMELETTE. As an example, a retina image is used. Two pipelines are compared:
# 1.) Simple OMELETTE with the Jerman filter and the non-iterative thresholding
# 2.) OMELETTE with the optional top-hat transfomation used as a preprocessing step and iterative thresholding
# Note: The goal is just to give a first intuition on how to use OMELETTE; this is not comparison of different set-ups

# imports
from omelette import hm_jerman, hm_filtered2skeleton, hm_filtered2skeleton_iterative, hm_overview_plot
from skimage import data
import numpy as np

# load 2D retina image and convert RGB to gray scale
img_rgb = data.retina()
img = 0.2125*img_rgb[:, :, 0] + 0.7154*img_rgb[:, :, 1] + 0.0721*img_rgb[:, :, 2]

# parameters used in both pipelines:
scales = np.arange(1.0, 5.5, 0.5)
tau = 1.0  # range 0.5..1.0; decrease tau to increase filter sensitivity

# 1.) Simple OMELETTE pipeline
# jerman filter
filtered, max_response = hm_jerman(img, scales, tau, black_ridges=True)
# convert to segmentation and skeleton
skeleton, segmentation = hm_filtered2skeleton(filtered)

# 2.) Advanced OMELETTE pipeline
# jerman filter with top-hat transformation
filtered_adv, max_response_adv = hm_jerman(img, scales, tau, black_ridges=True, do_tophat=True, tophat_size=9)
# for comparison: iterative approach
skeleton_adv, segmentation_adv = hm_filtered2skeleton_iterative(filtered_adv, n_threshold_iterations=20, kappa=0.01)

# plot the outputs
hm_overview_plot(img, filtered, segmentation, skeleton, max_response)
hm_overview_plot(img, filtered_adv, segmentation_adv, skeleton_adv, max_response_adv)

