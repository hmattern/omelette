import os
import numpy as np
import matplotlib.pyplot as plt
from skimage.filters import threshold_multiotsu, apply_hysteresis_threshold
from omelette import hm_load_nii_as_np_array, hm_load_h5_as_np_array


def _save_figure(filename):
    plt.savefig(filename,
                dpi=300,
                format='jpg',
                bbox_inches='tight')


def _sen_spe_bar_plot(current_axs, data, title):
    w = 0.8
    ind = (1, 2)
    # jerman
    current_axs.bar(ind[0], height=np.mean(data[:, 1]), width=w, yerr=np.std(data[:, 1]))
    # frangi
    current_axs.bar(ind[1], height=np.mean(data[:, 0]), width=w, yerr=np.std(data[:, 0]))

    # set x-labels, title etc.
    current_axs.set_xticks(ind)
    current_axs.set_xticklabels(('OMELETTE', 'Frangi-based\nbenchmark'))
    current_axs.set_title(title)
    current_axs.set_ylim([0.0, 1.05])


def _create_mip(image, mip_start, mip_end, projection_axis):
    if projection_axis == 0:
        mip = np.max(image[mip_start:mip_end, :, :], axis=projection_axis)
    elif projection_axis == 1:
        mip = np.max(image[:, mip_start:mip_end, :], axis=projection_axis)
    else:
        mip = np.max(image[:, :, mip_start:mip_end], axis=projection_axis)
    return np.rot90(mip)


def _plot_qualitative_comparison(path, sub_ind, fn_figure, fig_params, mip_start=1, mip_end=120, projection_axis=2):
    # load tof nii and window
    fn = os.path.join(path, 'sub' + sub_ind + '_tof_n4_brain.nii.gz')
    tof, _, _ = hm_load_nii_as_np_array(fn)
    percentile = np.percentile(tof, (55, 99.9))
    tof[tof < percentile[0]] = percentile[0]
    tof[tof > percentile[1]] = percentile[1]
    tof = (tof - np.min(tof)) / (np.max(tof) - np.min(tof))

    # load OMELETTE and benchmark filter and segmentation output
    fn = os.path.join(path, 'sub' + sub_ind + '_jerman_filt.nii.gz')
    jerman_filt, _, _ = hm_load_nii_as_np_array(fn)
    fn = os.path.join(path, 'sub' + sub_ind + '_frangi_filt.nii.gz')
    frangi_filt, _, _ = hm_load_nii_as_np_array(fn)
    fn = os.path.join(path, 'sub' + sub_ind + '_jerman_seg.nii.gz')
    jerman_seg, _, _ = hm_load_nii_as_np_array(fn)
    fn = os.path.join(path, 'sub' + sub_ind + '_frangi_seg.nii.gz')
    frangi_seg, _, _ = hm_load_nii_as_np_array(fn)

    # load semi-manual reference segmentation
    ref_seg = np.squeeze(hm_load_h5_as_np_array(os.path.join(path, 'sub' + sub_ind + '_tof_n4_brain_seg.h5')))
    ref_seg[ref_seg != 1] = 0  # set everything beside the vessels to zero

    # figure settings
    plt.rcParams.update(fig_params)
    fig, axs = plt.subplots(nrows=2, ncols=3, subplot_kw={'xticks': [], 'yticks': []})

    # plot
    axs[0, 0].imshow(_create_mip(tof, mip_start, mip_end, projection_axis), cmap='gray')
    axs[0, 0].set_title('Time-of-Flight\nvolume')
    axs[0, 1].imshow(_create_mip(jerman_filt, mip_start, mip_end, projection_axis), cmap='gray')
    axs[0, 1].set_title('OMELETTE:\nJerman enhancement')
    axs[0, 2].imshow(_create_mip(frangi_filt, mip_start, mip_end, projection_axis), cmap='gray')
    axs[0, 2].set_title('Benchmark:\nFrangi enhancement')

    axs[1, 0].imshow(_create_mip(ref_seg, mip_start, mip_end, projection_axis), cmap='gray')
    axs[1, 0].set_title('Semi-automatic\nreference segmentation')
    axs[1, 1].imshow(_create_mip(jerman_seg, mip_start, mip_end, projection_axis), cmap='gray')
    axs[1, 1].set_title('OMELETTE: Iterative\nhysteresis segmentation')
    axs[1, 2].imshow(_create_mip(frangi_seg, mip_start, mip_end, projection_axis), cmap='gray')
    axs[1, 2].set_title('Benchmark: Global\nthreshold segmentation')

    _save_figure(fn_figure)
    plt.show()


# # # # # # # # # # # # # # # # # # # # # # # #
# Iterative segmentation explanation
# # # # # # # # # # # # # # # # # # # # # # # #
do_iterative_explanation = False
if do_iterative_explanation:
    # set up parameters
    n_threshold_iterations = 3 #75
    kappa = 0.5 #0.05
    n_bins = 255
    slc_start = 300
    slc_end = 435
    proj_axis = 1

    plt.rcParams.update({'font.size': 16,
                         'legend.fontsize': 14,
                         'figure.figsize': [8.0, 4.0]})

    # load jerman enhanced
    sub_ind = '01'
    fn = os.path.join('/home/hendrik/hmwip/data/', 'sub' + sub_ind + '_jerman_filt.nii.gz')
    jerman_filt, _, _ = hm_load_nii_as_np_array(fn)

    # Iterative thresholding approach
    for i in range(0, n_threshold_iterations):
        # SAVE: enhanced image
        plt.imshow(_create_mip(jerman_filt, slc_start, slc_end, proj_axis), cmap='gray', interpolation='nearest')
        plt.axis('off')
        _save_figure('ISMRM_Fig_Explanation_Enhanced_Iter' + str(i+1) + '.jpg')
        plt.close()

        # Threshold from multi-otsu
        # below first/lower threshold is background, above second/higher threshold are (large/bright) vessels
        # small vessels lie in between thresholds
        thresholds = threshold_multiotsu(jerman_filt.ravel(), 3)

        # SAVE: histogram
        plt.hist(jerman_filt.ravel(), bins=n_bins)
        plt.yscale('log')
        plt.ylim((0.1, 10**8))
        for thresh in thresholds:
            plt.axvline(thresh, color='r')
        _save_figure('ISMRM_Fig_Explanation_Enhanced_LogHisto_Iter' + str(i + 1) + '.jpg')
        plt.close()

        # hysteresis thresholding based on multi-Otsu thresholds
        # this helps to find small vessels (in between thresholds)
        # which are connected to large/bright vessels (above second threshold)
        image_bin = apply_hysteresis_threshold(jerman_filt, thresholds[0], thresholds[1])

        # SAVE: segmentation with low and high voxels digitized
        plt.imshow(_create_mip(image_bin*np.digitize(jerman_filt, bins=thresholds), slc_start, slc_end, proj_axis),
                   cmap='hot')
        plt.axis('off')
        _save_figure('ISMRM_Fig_Explanation_Enhanced_SegDigit_Iter' + str(i + 1) + '.jpg')
        plt.close()

        # Increase intensity of segmented voxels and repeat iterative thresholding
        # This is similar to a compressed sensing approach and should help to disentangle vessels from background
        jerman_filt += image_bin * kappa
        jerman_filt[jerman_filt > 1.0] = 1.0  # cut-off out-of-range values


# # # # # # # # # # # # # # # # # # # # # # # #
# Quantitative segmentation comparison
# # # # # # # # # # # # # # # # # # # # # # # #
do_quantitative_comparison = True
if do_quantitative_comparison:
    dice = np.load('dice_artery_ISMRM.npy')
    sen = np.load('sen_artery_ISMRM.npy')
    spe = np.load('spe_artery_ISMRM.npy')

    ind = range(1, 3)

    plt.rcParams.update({'font.size': 16,
                         'legend.fontsize': 14,
                         'figure.figsize': [12.0, 4.0]})

    fig, axs = plt.subplots(ncols=3)
    _sen_spe_bar_plot(axs[0], sen, 'Sensitivity')
    _sen_spe_bar_plot(axs[1], spe, 'Specificity')
    _sen_spe_bar_plot(axs[2], dice, 'Dice coefficient')

    _save_figure('ISMRM_Fig_QuantEval.jpg')
    #plt.show()

    print('Jerman DICE: ' + str(np.mean(dice[:, 1])) + ' +/- ' + str(np.std(dice[:, 1])))
    print('Frangi DICE: ' + str(np.mean(dice[:, 0])) + ' +/- ' + str(np.std(dice[:, 0])))
    print('Jerman Sen: ' + str(np.mean(sen[:, 1])) + ' +/- ' + str(np.std(sen[:, 1])))
    print('Frangi Sen: ' + str(np.mean(sen[:, 0])) + ' +/- ' + str(np.std(sen[:, 0])))
    print('Jerman Spe: ' + str(np.mean(spe[:, 1])) + ' +/- ' + str(np.std(spe[:, 1])))
    print('Frangi Spe: ' + str(np.mean(spe[:, 0])) + ' +/- ' + str(np.std(spe[:, 0])))

# # # # # # # # # # # # # # # # # # # # # # # #
# Qualitative segmentation comparison
# # # # # # # # # # # # # # # # # # # # # # # #
do_qualitative_comparison = False
if do_qualitative_comparison:
    # Zoomed ROI:
    _plot_qualitative_comparison('/home/hendrik/hmwip/data/', '06', 'ISMRM_Fig_QualiEval_ROI.jpg',
                                 fig_params={'font.size': 10, 'figure.figsize': [12.0, 3.5]},
                                 mip_start=300, mip_end=435, projection_axis=1)
    # Whole Brain:
    _plot_qualitative_comparison('/home/hendrik/hmwip/data/', '01', 'ISMRM_Fig_QualiEval_Whole.jpg',
                                 fig_params={'font.size': 14, 'figure.figsize': [12.0, 9.5]},
                                 mip_start=1, mip_end=120, projection_axis=2)

