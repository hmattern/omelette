## ISMRM2021 
To reproduce the segmentation submitted to the 2021 ISMRM the following datasets need to be downloaded first:
- studyforrest angiograms: http://studyforrest.org/

Compared to the BVM2021 pipeline, a novel iterative thresholding approach was implemented.
The parameter-free, easy-to-tune thresholding is still supported (see `Example2D.py` and all the BVM scripts). 
The novel iterative thresholding is shown in `Example2Diterative.py`. 
All figures and results from sensitivity as well as specificity used in the ISMRM abstract are labeled with `*ISMRM*` in the filename.
 
Please note that the reference segmentations obtained by ilastik are not part of this repository to keep the required memory within a reasonable range (each segmentation is several hunderts of MB).  
The reference segmentations will be made available soon via external data hosting.

