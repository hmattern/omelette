# Script to segment the arteries from high resolution ToF images.
#
# StudyForrest (all 20 subjects) used. Data was bias field corrected using ANTs N4 correction.
# Data can be downloaded from: http://studyforrest.org/

import os
import numpy as np
from omelette import hm_load_nii_as_np_array, hm_save_np_array_as_nii  # nii handling
from omelette import hm_load_h5_as_np_array  # h5 handling
from omelette import hm_jerman, hm_filtered2skeleton_iterative  # proposed vessel segmentation
from skimage.filters import frangi  # state-of-the-art filter for comparison
# assess segmentation quality
from omelette import hm_plot_image_filter_comparison
from omelette import hm_segmentation_sensitivity, hm_segmentation_specificity, hm_dice_coefficient


# Data path; Please adapt accordingly
path = '/home/hendrik/hmwip/data/'

# Define filter parameters
scales = np.arange(1.0, 2.5, 0.5)
gamma = 0.05
threshold = 0.05
tau = 0.5
n_iteration = 50
kappa = 0.05

# Set up parameters
n_subjects = 20

dice = np.zeros((n_subjects, 2))
sen = np.zeros((n_subjects, 2))
spe = np.zeros((n_subjects, 2))

do_filtering = True
do_thresholding = True
do_comparison = True

# Iterate over all subjects
for i in range(1, n_subjects + 1):
    print('subject ' + str(i))

    # set subject index
    if i < 10:
        sub_ind = '0' + str(i)
    else:
        sub_ind = str(i)

    # load tof nii and normalize
    fn = os.path.join(path, 'sub' + sub_ind + '_tof_n4_brain.nii.gz')
    tof, affine, hdr = hm_load_nii_as_np_array(fn)
    # normalization:
    tof = (tof - np.min(tof)) / (np.max(tof) - np.min(tof))

    # do segmentation to get sensitivity and specificity
    if do_filtering:
        # jerman filtering
        tof_jerman_filt, _ = hm_jerman(tof, scales, tau)
        # frangi filter with normalization
        tof_frangi_filt = frangi(tof, sigmas=scales, gamma=gamma, black_ridges=False)
        tof_frangi_filt = tof_frangi_filt / np.max(tof_frangi_filt)

        # save
        fn = os.path.join(path, 'sub' + sub_ind + '_jerman_filt.nii.gz')
        hm_save_np_array_as_nii(tof_jerman_filt, affine, hdr, fn)
        fn = os.path.join(path, 'sub' + sub_ind + '_frangi_filt.nii.gz')
        hm_save_np_array_as_nii(tof_frangi_filt, affine, hdr, fn)

    else:
        fn = os.path.join(path, 'sub' + sub_ind + '_jerman_filt.nii.gz')
        tof_jerman_filt, _, _ = hm_load_nii_as_np_array(fn)
        fn = os.path.join(path, 'sub' + sub_ind + '_frangi_filt.nii.gz')
        tof_frangi_filt, _, _ = hm_load_nii_as_np_array(fn)

    if do_thresholding:
        # use emperically chosen threshold for frangi
        tof_frangi_seg = tof_frangi_filt > threshold
        # use new proposed threshold for jerman filtered
        _, tof_jerman_seg = hm_filtered2skeleton_iterative(tof_jerman_filt, n_iteration, kappa)

        # save filtered and segmented output as nii
        fn = os.path.join(path, 'sub' + sub_ind + '_jerman_seg.nii.gz')
        hm_save_np_array_as_nii(tof_jerman_seg, affine, hdr, fn)
        fn = os.path.join(path, 'sub' + sub_ind + '_frangi_seg.nii.gz')
        hm_save_np_array_as_nii(tof_frangi_seg, affine, hdr, fn)
    else:
        fn = os.path.join(path, 'sub' + sub_ind + '_jerman_seg.nii.gz')
        tof_jerman_seg, _, _ = hm_load_nii_as_np_array(fn)
        fn = os.path.join(path, 'sub' + sub_ind + '_frangi_seg.nii.gz')
        tof_frangi_seg, _, _ = hm_load_nii_as_np_array(fn)

    # do comparison
    if do_comparison:
        # load semi-manual reference segmentation
        ref_seg = np.squeeze(hm_load_h5_as_np_array(os.path.join(path, 'sub' + sub_ind + '_tof_n4_brain_seg.h5')))
        ref_seg[ref_seg != 1] = 0  # set everything beside the vessels to zero

        # do image-base filter comparison
        #   window tof
        percentile = np.percentile(tof, (50, 99.9))
        tof[tof < percentile[0]] = percentile[0]
        tof[tof > percentile[1]] = percentile[1]
        tof = (tof - np.min(tof)) / (np.max(tof) - np.min(tof))
        #   plot
        hm_plot_image_filter_comparison(tof, ref_seg, tof_frangi_filt, tof_frangi_seg,
                                        tof_jerman_filt, tof_jerman_seg, title='TOF_TRA_sub' + sub_ind,
                                        mip_start=1, mip_end=120, projection_axis=2)

        dice[i - 1, 0] = hm_dice_coefficient(ref_seg, tof_frangi_seg)
        dice[i - 1, 1] = hm_dice_coefficient(ref_seg, tof_jerman_seg)
        sen[i-1, 0] = hm_segmentation_sensitivity(ref_seg, tof_frangi_seg)
        sen[i-1, 1] = hm_segmentation_sensitivity(ref_seg, tof_jerman_seg)
        spe[i-1, 0] = hm_segmentation_specificity(ref_seg, tof_frangi_seg)
        spe[i-1, 1] = hm_segmentation_specificity(ref_seg, tof_jerman_seg)

np.save('dice_artery_ISMRM.npy', dice)
np.save('sen_artery_ISMRM.npy', sen)
np.save('spe_artery_ISMRM.npy', spe)
