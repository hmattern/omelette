import os
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from skimage import exposure, morphology
from omelette import hm_jerman, hm_filtered2skeleton_iterative
from omelette import hm_dice_coefficient


def print_optimal_setting():
    d_arg, g_arg, t_arg, i_arg, c_arg = np.unravel_index(np.argmax(mean_dice), mean_dice.shape)
    print("optimal setting dice = ", str(np.max(mean_dice)))
    print("optimal disk size = ", str(disk_sizes[d_arg]))
    print("optimal gamma = ", str(gammas[g_arg]))
    print("optimal tau = ", str(taus[t_arg]))
    print("optimal iterations = ", str(iterations[i_arg]))
    print("optimal cut-off = ", str(pruning_cut_offs[c_arg]))
    print("---------------------")


# define parameters
subject_range = range(21, 41)

sigmas = np.arange(1.0, 3.1, 1.0)
taus = [0.75]  # taus = [0.5, 0.75, 1.0]
iterations = [1]  # iterations = [1, 5, 10, 25, 50, 100, 200]
kappa = 0.05
pruning_cut_offs = [100] # pruning_cut_offs = [50, 100, 150]
gammas = [1.0]  # gammas = [0.7, 1.0, 1.2]
disk_sizes = [5]  # disk_sizes = [4, 5, 6, 7]

compute_dice = True
plot_active = False

if compute_dice:
    dice = np.zeros((len(subject_range), len(disk_sizes), len(gammas), len(taus), len(iterations), len(pruning_cut_offs)))

    # loop over all subjects
    for ind, s in zip(subject_range, range(0, len(subject_range))):
        print(ind)
        # load image
        image_rgb = np.array(Image.open(os.path.join(os.getcwd(), "training/images/" + str(ind) + "_training.tif")))
        mask = np.array(Image.open(os.path.join(os.getcwd(), "training/mask/" + str(ind) + "_training_mask.gif")))
        ground_truth = np.array(Image.open(os.path.join(os.getcwd(), "training/1st_manual/" + str(ind) + "_manual1.gif")))

        # convert RGB image to gray scale
        image_org = 0.2125 * image_rgb[:, :, 0] + 0.7154 * image_rgb[:, :, 1] + 0.0721 * image_rgb[:, :, 2]
        # normalize image
        image_org = image_org/np.max(image_org)

        # loop over all disk sizes for the tophat transform
        for disk_size, d in zip(disk_sizes, range(0, len(disk_sizes))):
            # tophat transform
            image = morphology.black_tophat(image_org, selem=morphology.disk(disk_size))

            # loop over all gammas
            for gamma, g in zip(gammas, range(0, len(gammas))):
                # gamma correction
                image = exposure.adjust_gamma(image, gamma)

                # normalize ground_truth
                ground_truth = ground_truth/np.max(ground_truth)

                # loop over all filter settings
                for tau, t in zip(taus, range(0, len(taus))):
                    # vesselness enhancement
                    filtered, response = hm_jerman(image, sigmas, tau, black_ridges=False)

                    # apply mask to filtered image (mask part of dataset)
                    filtered = filtered * (mask > 0)

                    # loop over all iteration settiongs
                    for iteration, i in zip(iterations, range(0, len(iterations))):

                        # loop over all pruning cut-offs
                        for cut_off, c in zip(pruning_cut_offs, range(0, len(pruning_cut_offs))):
                            # segmentation
                            skeleton, segmentation = hm_filtered2skeleton_iterative(filtered, iteration, kappa, cut_off)

                            # DICE
                            dice[s, d, g, t, i, c] = hm_dice_coefficient(ground_truth, segmentation)

                            if plot_active:
                                # plot
                                fig, axs = plt.subplots(nrows=2, ncols=3)
                                axs[0, 0].imshow(image_rgb)
                                axs[0, 1].imshow(image, cmap="gray")
                                axs[0, 2].imshow(ground_truth, cmap="gray")
                                axs[1, 0].imshow(filtered, cmap="gray")
                                axs[1, 1].imshow(segmentation, cmap="gray")
                                axs[1, 2].imshow(ground_truth-segmentation, cmap="gray")
                                plt.show()

    np.save('dice_tophat.npy', dice)
else:
    dice = np.load('dice_tophat.npy')

mean_dice = np.mean(dice, axis=0)
print_optimal_setting()

mean_dice[mean_dice == np.max(mean_dice)] = 0
print_optimal_setting()

mean_dice[mean_dice == np.max(mean_dice)] = 0
print_optimal_setting()
