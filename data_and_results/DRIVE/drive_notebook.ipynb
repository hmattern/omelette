{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "from PIL import Image\n",
    "from skimage.filters import threshold_multiotsu, apply_hysteresis_threshold\n",
    "from skimage.filters.ridges import compute_hessian_eigenvalues\n",
    "from skimage.morphology import skeletonize, remove_small_objects, black_tophat, white_tophat, disk, ball\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def _hm_jerman_vesselness_response(hessian_eigenvalues, black_ridges=False, tau=0.5):\n",
    "    # extract the second eigenvalues\n",
    "    lambda2 = hessian_eigenvalues[1, :, :]\n",
    "\n",
    "    # 2D vs 3D handling\n",
    "    if len(hessian_eigenvalues) == 2:\n",
    "        lambda_rho = np.copy(lambda2)  # if 2D use lambda2; copy require, otherwise only reference\n",
    "    else:\n",
    "        lambda_rho = hessian_eigenvalues[2, :, :]  # if 3D use lambda3; hence using reference is okay here\n",
    "\n",
    "    # bright ridges require \"sign switch\"\n",
    "    if not black_ridges:\n",
    "        lambda2 = -lambda2\n",
    "        lambda_rho = -lambda_rho\n",
    "\n",
    "    # computes lambda_rho: regularized version of lambda2/3 to prevent noise enhancement for low responses\n",
    "    threshold = tau * np.max(lambda_rho)  # compute threshold\n",
    "    lambda_rho[(lambda_rho > 0) & (lambda_rho <= threshold)] = threshold  # \"boost\" pos. values smaller than threshold\n",
    "    lambda_rho[lambda_rho <= 0] = 0  # set negative values to zero\n",
    "\n",
    "    # calculate vessel response\n",
    "    denominator = ((lambda2 + lambda_rho)**3)\n",
    "    denominator[denominator == 0] = 1e-6  # prevent division by zero\n",
    "    response = 27 * (lambda_rho - lambda2) * lambda2**2 / denominator\n",
    "    # account for structures with elliptic cross-sections with ratio of lambda2/lambda_rho from 0.5 to 1\n",
    "    response[(lambda2 >= lambda_rho / 2) & (lambda_rho > 0)] = 1\n",
    "    # suppress negative eigenvalues\n",
    "    response[(lambda2 <= 0) | (lambda_rho <= 0)] = 0\n",
    "\n",
    "    return response\n",
    "\n",
    "\n",
    "def _hm_plot_hessian_eigenvalues(hessian_eigenvalues):\n",
    "    cmap = plt.get_cmap('gray')\n",
    "    if hessian_eigenvalues.shape[0] == 2:\n",
    "        _, ax = plt.subplots(1, 2, figsize=(8, 4))\n",
    "        ax[0].imshow(hessian_eigenvalues[0, :, :], cmap=cmap)\n",
    "        ax[1].imshow(hessian_eigenvalues[1, :, :], cmap=cmap)\n",
    "    else:\n",
    "        _, ax = plt.subplots(1, 3, figsize=(8, 4))\n",
    "        ax[0].imshow(hessian_eigenvalues[0, :, :], cmap=cmap)\n",
    "        ax[1].imshow(hessian_eigenvalues[1, :, :], cmap=cmap)\n",
    "        ax[2].imshow(hessian_eigenvalues[2, :, :], cmap=cmap)\n",
    "    plt.show()\n",
    "\n",
    "\n",
    "def hm_jerman(image, sigmas=range(1, 10, 2), tau=0.5, black_ridges=False,\n",
    "              do_tophat=False, tophat_size=None,\n",
    "              mode='reflect', cval=0, verbose=False):\n",
    "    \"\"\"\n",
    "    Filter an image with the Jerman vesselness filter [1]. Available for MATLAB here [2].\n",
    "    Related to the popular Frangi vesselness approach [3].\n",
    "    This filter is based on the analysis of the eigenvalues of the Hessian matrix of an image and\n",
    "    can be used to detect continuous ridges, e.g. vessels, wrinkles, rivers.\n",
    "    Defined only for 2-D and 3-D images.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    image : (N, M[, P]) ndarray\n",
    "        Array with input image data [2D or 3D].\n",
    "        This image will be normalized to [0..1].\n",
    "    sigmas : iterable of floats, optional\n",
    "        Sigmas used as scales of filter, i.e.,\n",
    "        np.arange(scale_range[0], scale_range[1], scale_step)\n",
    "    tau : float [0.5..1.0], optional\n",
    "        Parameter used for the regularization.\n",
    "        Lower values result in increased response (also for noise)\n",
    "    black_ridges : boolean, optional\n",
    "        When True, the filter detects black ridges;\n",
    "        When False, it detects white ridges (default).\n",
    "    do_tophat: boolean, optional\n",
    "        When True, a top-hat transformation is performed as a pre-processing step to isolate the vasculature.\n",
    "    tophat_size: integer, optional\n",
    "        Determines the size of the structured element used in the top-hat transformation.\n",
    "        Features larger than this structured element are filtered out\n",
    "        Thus, set the size larger than the highest sigma used.\n",
    "        If not specified, an tophat_size is computed automatically based on the inputed sigmas\n",
    "    mode : {'constant', 'reflect' (default), 'wrap', 'nearest', 'mirror'}, optional\n",
    "        How to handle values outside the image borders.\n",
    "        The use of 'reflect' aka default is recommended\n",
    "    cval : float, optional\n",
    "        Used in conjunction with mode 'constant', the value outside\n",
    "        the image boundaries.\n",
    "    verbose : bool, optional\n",
    "        If true the Hessian eigenvalues are plotted per scale.\n",
    "    Returns\n",
    "    -------\n",
    "    filtered_image: (N, M[, P]) ndarray\n",
    "        Filtered image (maximum of pixels across all scales).\n",
    "    max_response_sigma: (N, M[, P]) ndarray\n",
    "        Maximal response/sigma for each pixel across all scales.\n",
    "\n",
    "    Notes\n",
    "    -----\n",
    "    Proposed by T. Jerman et al in [1]_\n",
    "    MATLAB implementation in [2]_\n",
    "    Re-written for Python by Hendrik Mattern, August 2020\n",
    "    The code relies heavily on the scikit-image, and also the code structure is largely following the scikit-image\n",
    "    Frangi implementation (very similar up to the response function, which is re-implemented from Jerman et al.)\n",
    "\n",
    "    References\n",
    "    ----------\n",
    "    .. [1] T. Jerman, F. Pernus, B. Likar, Z. Spiclin,\n",
    "        \"Enhancement of Vascular Structures in 3D and 2D Angiographic Images\",\n",
    "        IEEE Transactions on Medical Imaging, 35(9), p. 2107-2118 (2016), doi=10.1109/TMI.2016.2550102\n",
    "    .. [2] T. Jerman: https://github.com/timjerman/JermanEnhancementFilter\n",
    "    .. [3] Frangi, A. F., Niessen, W. J., Vincken, K. L., & Viergever, M. A.\n",
    "        (1998,). Multiscale vessel enhancement filtering. In International\n",
    "        Conference on Medical Image Computing and Computer-Assisted\n",
    "        Intervention (pp. 130-137). Springer Berlin Heidelberg.\n",
    "        :DOI:`10.1007/BFb0056195`\n",
    "    \"\"\"\n",
    "\n",
    "    # Check image dimensions (could also be done with private method <check_nD(image, [2, 3])> )\n",
    "    if not len(image.shape) in [2, 3]:\n",
    "        raise ValueError('image is not 2D or 3D')\n",
    "\n",
    "    # Check sigma scales (could also be done with private method <sigmas = _check_sigmas(sigmas)> )\n",
    "    if np.any(np.asarray(sigmas).ravel() < 0.0):\n",
    "        raise ValueError('Sigma values should be non-negative.')\n",
    "\n",
    "    # Normalize input image\n",
    "    image = (image - np.min(image)) / (np.max(image) - np.min(image))\n",
    "\n",
    "    # Top-hat transformation (optional pre-processing:\n",
    "    if do_tophat:\n",
    "        # set default size of top-hat transformation if required:\n",
    "        if tophat_size is None:\n",
    "            tophat_size = np.int_(sigmas[-1] + 5.0)\n",
    "        # create structure element to perform the transformation with\n",
    "        if len(image.shape) == 2:\n",
    "            structure_element = disk(tophat_size)\n",
    "        else:\n",
    "            structure_element = ball(tophat_size)\n",
    "        # do black or white top-hat transformation depending on the rigid-flag\n",
    "        # FYI: after the top-hat transformation the output is hyperintense, aka white vessels\n",
    "        # Hence, adapt black_ridges flag if required\n",
    "        if black_ridges:\n",
    "            image = black_tophat(image, selem=structure_element)\n",
    "            black_ridges = False\n",
    "        else:\n",
    "            image = white_tophat(image, selem=structure_element)\n",
    "\n",
    "    # Initialize filtered image\n",
    "    filtered_image = np.zeros(image.shape)\n",
    "\n",
    "    # Initialize array which stores scale of the maximum response (can be used to approx. vessel diameter)\n",
    "    max_response_sigma = np.zeros(image.shape)\n",
    "\n",
    "    # Filtering for all (sigma) scales\n",
    "    for sigma in sigmas:\n",
    "        print('current sigma: ', str(sigma))\n",
    "        # Compute the eigenvalues of the Hessian (ie. lambda 1, 2 (and 3))\n",
    "        #   1. Compute the Hessian of the image (use xy instead of rc order)\n",
    "        #   2. Correct for scale\n",
    "        #   3. Get eigenvalues\n",
    "        #   4. Sort by absolute value\n",
    "        hessian_eigenvalues = compute_hessian_eigenvalues(image=image, sigma=sigma, sorting='abs', mode=mode, cval=cval)\n",
    "\n",
    "        if verbose:\n",
    "            _hm_plot_hessian_eigenvalues(hessian_eigenvalues)\n",
    "\n",
    "        # Compute vesselness response function\n",
    "        response = _hm_jerman_vesselness_response(hessian_eigenvalues, black_ridges=black_ridges, tau=tau)\n",
    "\n",
    "        # enhanced output is the maximum across all scales\n",
    "        filtered_image = np.maximum(response, filtered_image)\n",
    "\n",
    "        # get scale of maximum response for diameter approximation\n",
    "        #   (if current scale is current maximum, replace the sigma entry in max_scale_per_voxel\n",
    "        max_response_sigma[filtered_image == response] = sigma\n",
    "\n",
    "    # Return for every pixel the maximum value over all (sigma) scales\n",
    "    return filtered_image, max_response_sigma"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def _hm_pruning(data, pruning_cut_off=0):\n",
    "    if pruning_cut_off > 0:\n",
    "        remove_small_objects(data, pruning_cut_off, connectivity=len(data.shape), in_place=True)\n",
    "    return data\n",
    "\n",
    "def hm_filtered2skeleton(image_filtered, pruning_cut_off=0, verbose_plot=False):\n",
    "    # 0. Init verbose plotting\n",
    "    if verbose_plot:\n",
    "        n_col = 2\n",
    "        n_row = 2\n",
    "        _, axs = plt.subplots(ncols=n_col, nrows=n_row, sharey=True)\n",
    "        axs = axs.ravel()\n",
    "        _hm_plotting_processing(axs, n_col*n_row, 0, image_filtered, 'unprocessed vesselness image')\n",
    "\n",
    "    # 1. Threshold from multi-otsu\n",
    "    # below first threshold is background, above last threshold are (large/bright) vessels\n",
    "    # small vessels lie in between thresholds\n",
    "    # FYI: consider only non-zero values\n",
    "    thresholds = threshold_multiotsu(image_filtered[image_filtered != 0].ravel(), 3)\n",
    "\n",
    "    if verbose_plot:\n",
    "        _hm_plotting_processing(axs, n_col*n_row, 1, np.digitize(image_filtered, bins=thresholds),\n",
    "                                'multi-otus-threshold')\n",
    "\n",
    "    # 2. Hysteresis thresholding based on multi-Otsu threshold of segmentation\n",
    "    # this helps to find small vessels (in between thresholds)\n",
    "    # which are connected to large/bright vessels (above second threshold)\n",
    "    image_bin = apply_hysteresis_threshold(image_filtered, thresholds[0], thresholds[1])\n",
    "\n",
    "    # 3. Pruning\n",
    "    image_bin = _hm_pruning(image_bin, pruning_cut_off)\n",
    "\n",
    "    if verbose_plot:\n",
    "        _hm_plotting_processing(axs, n_col*n_row, 2, image_bin, 'binary image after hysteresis thresholding')\n",
    "\n",
    "    # 4. Skeletonize\n",
    "    # method='lee' will be used automatically for 3D input\n",
    "    image_skeleton = skeletonize(image_bin)\n",
    "\n",
    "    if verbose_plot:\n",
    "        _hm_plotting_processing(axs, n_col*n_row, 3, image_skeleton, 'skeleton')\n",
    "        plt.show()\n",
    "\n",
    "    return image_skeleton, image_bin"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "01\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "02\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "03\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "04\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "05\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "06\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "07\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "08\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "09\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "10\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "11\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "12\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "13\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "14\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "15\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "16\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "17\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "18\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "19\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n",
      "20\n",
      "current sigma:  1.0\n",
      "current sigma:  2.0\n",
      "current sigma:  3.0\n"
     ]
    }
   ],
   "source": [
    "# define parameters\n",
    "subject_range = range(1, 21)\n",
    "\n",
    "sigmas = np.arange(1.0, 3.1, 1.0)\n",
    "tau = 0.75\n",
    "cut_off = 100\n",
    "disk_size = 5\n",
    "\n",
    "\n",
    "# loop over all subjects\n",
    "for s in subject_range:\n",
    "    if s < 10:\n",
    "        ind = \"0\" + str(s)\n",
    "    else:\n",
    "        ind = str(s)\n",
    "    print(ind)\n",
    "    # load image\n",
    "    image_rgb = np.array(Image.open(os.path.join(os.getcwd(), \"test/images/\" + str(ind) + \"_test.tif\")))\n",
    "    mask = np.array(Image.open(os.path.join(os.getcwd(), \"test/mask/\" + str(ind) + \"_test_mask.gif\")))\n",
    "\n",
    "    # convert RGB image to gray scale\n",
    "    image = 0.2125 * image_rgb[:, :, 0] + 0.7154 * image_rgb[:, :, 1] + 0.0721 * image_rgb[:, :, 2]\n",
    "    # normalize image\n",
    "    image = image/np.max(image)\n",
    "\n",
    "    # vessel enhancement\n",
    "    filtered, _ = hm_jerman(image, sigmas, tau, black_ridges=True, do_tophat=True, tophat_size=disk_size)\n",
    "\n",
    "    # apply mask to filtered image (mask part of dataset)\n",
    "    filtered = filtered * (mask > 0)\n",
    "\n",
    "    # create segmentation\n",
    "    _, segmentation = hm_filtered2skeleton(filtered, cut_off)\n",
    "                                   \n",
    "    # save as binary png\n",
    "    im = Image.fromarray(segmentation>0)\n",
    "    im.save(str(s) + \".png\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "omelette",
   "language": "python",
   "name": "omelette"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
