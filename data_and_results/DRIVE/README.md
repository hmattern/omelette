## Results for DRIVE challenge

https://drive.grand-challenge.org/

pillow (former PIL) is required to handle png/tiff/gif images.
Average Dice score for testing set: 0.7646.
Jupyter notebook (used for submission) can be found under `data_and_results\drive_notebook.ipynb`.