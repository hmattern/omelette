import os
import numpy as np
import matplotlib.pyplot as plt
from omelette import hm_jerman, hm_filtered2skeleton_iterative
from omelette import hm_load_nii_as_np_array, hm_save_np_array_as_nii
from omelette import hm_create_mip, hm_create_minip


def image2segmentation(folder, subject, contrast, taus, scale_min, scale_max, scale_step,
                       tophat_sizes, threshold_iterations):
    # load
    if contrast == "tof":
        subfix = '_n4.nii.gz'
    else:
        subfix = '.nii'
    data, affine, hdr = hm_load_nii_as_np_array(os.path.join(folder, subject + "_" + contrast + subfix))

    # normalize
    #if contrast == "qsm":
    if contrast == "NOTUSED":
        data[data < 0.0] = 0.0
        data = data / 0.2
        data[data > 1.0] = 1.0
    else:
        data = (data - np.min(data)) / (np.max(data) - np.min(data))

    # filter data
    if contrast == "swi":
        black_ridges = True
    else:
        black_ridges = False
    for tophat_size in tophat_sizes:
        if tophat_size == 0:
            do_tophat = False
        else:
            do_tophat = True
        for tau in taus:
            filtered, response = hm_jerman(data, np.arange(scale_min, scale_max, scale_step),
                                           tau, black_ridges=black_ridges,
                                           do_tophat=do_tophat, tophat_size=tophat_size)
            # save
            param_str = "_tau_" + str(tau) + "_hat_" + str(tophat_size) + "v2.nii.gz"
            hm_save_np_array_as_nii(filtered, affine, hdr,
                                    os.path.join(folder, subject + "_" + contrast + "_filt" + param_str))
            hm_save_np_array_as_nii(response, affine, hdr,
                                    os.path.join(folder, subject + "_" + contrast + "_resp" + param_str))

            # thresholding:
            for t in threshold_iterations:
                skeleton, segmentation = hm_filtered2skeleton_iterative(filtered, t, kappa=0.05, pruning_cut_off=50)
                # save
                param_str = "_tau_" + str(tau) + "_hat_" + str(tophat_size) + "_iter_" + str(t) + "v2.nii.gz"
                hm_save_np_array_as_nii(skeleton, affine, hdr,
                                        os.path.join(folder, subject + "_" + contrast + "_skel" + param_str))
                hm_save_np_array_as_nii(segmentation, affine, hdr,
                                        os.path.join(folder, subject + "_" + contrast + "_segm" + param_str))

                # plot settings (much hacky... such bad style...)
                if contrast == "tof":
                    mip_start_ax = 0
                    mip_end_ax = int(data.shape[2])
                    mip_start_cor = int(0.4 * data.shape[1])
                    mip_end_cor = int(0.75 * data.shape[1])
                else:
                    mip_start_ax = int(0.45 * data.shape[2])
                    mip_end_ax = int(0.46 * data.shape[2])
                    mip_start_cor = int(0.40 * data.shape[1])
                    mip_end_cor = int(0.41 * data.shape[1])

                # do plot
                fig, axs = plt.subplots(ncols=3, nrows=2, subplot_kw={'xticks': [], 'yticks': []})
                cmap_str = "gray"

                if contrast == "swi":
                    axs[0, 0].imshow(hm_create_minip(data, mip_start_ax, mip_end_ax, projection_axis=2), cmap=cmap_str)
                    axs[1, 0].imshow(hm_create_minip(data, mip_start_cor, mip_end_cor, projection_axis=1),
                                     cmap=cmap_str)
                else:
                    axs[0, 0].imshow(hm_create_mip(data, mip_start_ax, mip_end_ax, projection_axis=2), cmap=cmap_str)
                    axs[1, 0].imshow(hm_create_mip(data, mip_start_cor, mip_end_cor, projection_axis=1), cmap=cmap_str)

                axs[0, 1].imshow(hm_create_mip(filtered, mip_start_ax, mip_end_ax, projection_axis=2), cmap=cmap_str)

                axs[0, 2].imshow(hm_create_mip(segmentation, mip_start_ax, mip_end_ax, projection_axis=2),
                                 cmap=cmap_str)

                axs[1, 1].imshow(hm_create_mip(filtered, mip_start_cor, mip_end_cor, projection_axis=1),
                                 cmap=cmap_str)
                axs[1, 2].imshow(hm_create_mip(segmentation, mip_start_cor, mip_end_cor, projection_axis=1),
                                 cmap=cmap_str)

                # save plot
                param_str = "_tau_" + str(tau) + "_hat_" + str(tophat_size) + "_iter_" + str(t) + "v2.jpg"
                plt.savefig(os.path.join(folder, subject + "_" + contrast + "_over" + param_str),
                            dpi=300,
                            format='jpg',
                            bbox_inches='tight')
                plt.close(fig)


# define parameters
path = 'Y:\\VDM_processing_outsource\\studyforrest_2020_09_29'
n_subjects = 20

# Iterate over all subjects
for s in range(1, n_subjects + 1):
    print('subject ' + str(s))

    # set subject index
    if s < 10:
        sub_ind = 'sub0' + str(s)
    else:
        sub_ind = 'sub' + str(s)

    # image2segmentation(path, sub_ind, "tof",
    #                    taus=[0.5, 0.75],
    #                    scale_min=1.0, scale_max=3.6, scale_step=0.5,
    #                    tophat_sizes=[4],
    #                    threshold_iterations=[15])
    #
    # image2segmentation(path, sub_ind, "swi",
    #                    taus=[0.75, 1.0],
    #                    scale_min=1.0, scale_max=2.1, scale_step=0.5,
    #                    tophat_sizes=[3],
    #                    threshold_iterations=[1])

    image2segmentation(path, sub_ind, "qsm",
                       taus=[0.5, 0.75, 1.0],
                       scale_min=1.0, scale_max=2.1, scale_step=0.5,
                       tophat_sizes=[3],
                       threshold_iterations=[1])
