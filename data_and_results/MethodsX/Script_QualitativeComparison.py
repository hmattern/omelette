import os
import numpy as np
from omelette import hm_load_nii_as_np_array, hm_save_np_array_as_nii  # io NifTi files
from omelette import hm_jerman, hm_filtered2skeleton_iterative, hm_filtered2skeleton  # enhancement and segmentation
from skimage.filters import frangi  # for the benchmark segmentation


def image2segmentation(filename_nii, sigmas, tau, top_hat_size, n_threshold, kappa):
    # Load nii-file
    data, affine, header = hm_load_nii_as_np_array(filename_nii)

    # Normalize
    data = (data - np.min(data)) / (np.max(data) - np.min(data))

    # Top-hat transformation only done if structuring element size > 0
    if top_hat_size == 0:
        do_top_hat = False
    else:
        do_top_hat = True
    # Filter the data
    enhanced, _ = hm_jerman(data, sigmas, tau, black_ridges=False, do_tophat=do_top_hat, tophat_size=top_hat_size)

    # Segmentation
    if n_threshold > 1:
        _, segmentation = hm_filtered2skeleton_iterative(enhanced, n_threshold_iterations=n_threshold, kappa=kappa,
                                                         pruning_cut_off=50)
    else:
        _, segmentation = hm_filtered2skeleton(enhanced, pruning_cut_off=50)

    # Save results as nii-files
    hm_save_np_array_as_nii(enhanced, affine, header, filename_nii.split(".nii")[0] + "_enhanced.nii")
    hm_save_np_array_as_nii(segmentation, affine, header, filename_nii.split(".nii")[0] + "_segmentation.nii")

    return enhanced, segmentation


def image2segmentation_benchmark(filename_nii, sigmas, gamma, threshold):
    # Load nii-file
    data, affine, header = hm_load_nii_as_np_array(filename_nii)
    # Normalize
    data = (data - np.min(data)) / (np.max(data) - np.min(data))
    # Filter the data
    enhanced = frangi(data, sigmas, gamma=gamma, black_ridges=False)
    # Segmention
    segmentation = enhanced > threshold
    # Save results as nii-files
    hm_save_np_array_as_nii(enhanced, affine, header, filename_nii.split(".nii")[0] + "_enhanced_bench.nii")
    hm_save_np_array_as_nii(segmentation, affine, header, filename_nii.split(".nii")[0] + "_segmentation_bench.nii")

    return enhanced, segmentation


# Script to segment vessel in high resolution MRI data
path_data = 'Y:/OMELETTE_paper/data'  # adapt accordingly

enh_qsm, seg_qsm = image2segmentation(os.path.join(path_data, 'qsm330.nii'),
                                      sigmas=np.arange(1.0, 2.1, 1.0), tau=0.75, top_hat_size=3,
                                      n_threshold=0, kappa=0.0)
enh_qsm_frangi, seg_qsm_frangi = image2segmentation_benchmark(os.path.join(path_data, 'qsm330.nii'),
                                                              sigmas=np.arange(1.0, 2.1, 1.0), gamma=0.05,
                                                              threshold=0.05)

enh_tof, seg_tof = image2segmentation(os.path.join(path_data, 'tof150.nii'),
                                      sigmas=np.arange(1.0, 3.1, 1.0), tau=0.70, top_hat_size=4,
                                      n_threshold=5, kappa=0.15)
enh_tof_frangi, seg_tof_frangi = image2segmentation_benchmark(os.path.join(path_data, 'tof150.nii'),
                                                              sigmas=np.arange(1.0, 3.1, 1.0), gamma=0.025,
                                                              threshold=0.2)
