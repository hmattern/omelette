import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from omelette import hm_jerman, hm_filtered2skeleton  # vessel enhancement and segmentation


def plot_and_save(img, fn_save, cmap_str="gray"):
    fig, ax = plt.subplots()
    ax.imshow(img, cmap=cmap_str)
    ax.set_axis_off()
    plt.savefig(fn_save, dpi=120, format='png', bbox_inches='tight', pad_inches=0)


# load image, mask, and ground truth
image_rgb = np.array(Image.open("../DRIVE/test/images/02_test.tif"))
mask = np.array(Image.open("../DRIVE/test/mask/02_test_mask.gif"))

# convert RGB image to gray scale
image = 0.2125 * image_rgb[:, :, 0] + 0.7154 * image_rgb[:, :, 1] + 0.0721 * image_rgb[:, :, 2]
# normalize image
image = image / np.max(image)

# vessel enhancement with top-hat transformation as pre-processing
enhanced, response = hm_jerman(image, np.arange(1.0, 3.1, 1.0), tau=0.75, black_ridges=True,
                               do_tophat=True, tophat_size=5)

# apply mask to filtered image (mask part of dataset)
enhanced = enhanced * (mask > 0)

# vessel segmentation with optional pruning to remove small noise blobs
skeleton, segmentation = hm_filtered2skeleton(enhanced, pruning_cut_off=100)

# plot and save
plot_and_save(image, "drive_image.png")
plot_and_save(enhanced, "drive_enhanced.png")
plot_and_save(segmentation, "drive_segmentation.png")
plot_and_save(skeleton*response/np.max(skeleton*response), "drive_skeleton_with_response.png", cmap_str="hot")

