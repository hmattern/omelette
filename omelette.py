import numpy as np
import matplotlib.pyplot as plt
import nibabel as nib
import h5py

from skimage.filters import threshold_multiotsu, apply_hysteresis_threshold
from skimage.filters.ridges import compute_hessian_eigenvalues
from skimage.morphology import skeletonize, remove_small_objects, black_tophat, white_tophat, disk, ball
# todo
# from skimage.measure import label, regionprops


def hm_load_nii_as_np_array(filename):
    nii_file = nib.load(filename)
    return np.squeeze(nii_file.get_fdata()), nii_file.affine, nii_file.header


def hm_save_np_array_as_nii(np_array, affine, header, filename):
    nii = nib.Nifti1Image(np_array, affine, header)
    nib.save(nii, filename)


def hm_load_h5_as_np_array(fn_h5, dataset_name=None):
    f = h5py.File(fn_h5, 'r')

    if dataset_name is None:
        dset = f[list(f.keys())[0]]
    else:
        dset = f[dataset_name]

    return dset[()]


def _hm_remove_ext_from_fn(fn, ext='.nii.gz'):
    return fn.split(ext)[0]


def hm_nii_to_h5(fn_nii, ext_nii='.nii.gz'):
    # load nii
    data, affine, hdr = hm_load_nii_as_np_array(fn_nii)

    # create empty h5-file
    f = h5py.File(_hm_remove_ext_from_fn(fn_nii, ext_nii) + '.h5', "w")

    # write h5 file; "chunk-ing" can improve ilastik performance
    dset = f.create_dataset("hm_data", data=data, chunks=True)
    f.close()


def _hm_jerman_vesselness_response(hessian_eigenvalues, black_ridges=False, tau=0.5):
    # extract the second eigenvalues
    lambda2 = hessian_eigenvalues[1, :, :]

    # 2D vs 3D handling
    if len(hessian_eigenvalues) == 2:
        lambda_rho = np.copy(lambda2)  # if 2D use lambda2; copy require, otherwise only reference
    else:
        lambda_rho = hessian_eigenvalues[2, :, :]  # if 3D use lambda3; hence using reference is okay here

    # bright ridges require "sign switch"
    if not black_ridges:
        lambda2 = -lambda2
        lambda_rho = -lambda_rho

    # computes lambda_rho: regularized version of lambda2/3 to prevent noise enhancement for low responses
    threshold = tau * np.max(lambda_rho)  # compute threshold
    lambda_rho[(lambda_rho > 0) & (lambda_rho <= threshold)] = threshold  # "boost" pos. values smaller than threshold
    lambda_rho[lambda_rho <= 0] = 0  # set negative values to zero

    # calculate vessel response
    denominator = ((lambda2 + lambda_rho)**3)
    denominator[denominator == 0] = 1e-6  # prevent division by zero
    response = 27 * (lambda_rho - lambda2) * lambda2**2 / denominator
    # account for structures with elliptic cross-sections with ratio of lambda2/lambda_rho from 0.5 to 1
    response[(lambda2 >= lambda_rho / 2) & (lambda_rho > 0)] = 1
    # suppress negative eigenvalues
    response[(lambda2 <= 0) | (lambda_rho <= 0)] = 0

    return response


def _hm_plot_hessian_eigenvalues(hessian_eigenvalues):
    cmap = plt.get_cmap('gray')
    if hessian_eigenvalues.shape[0] == 2:
        _, ax = plt.subplots(1, 2, figsize=(8, 4))
        ax[0].imshow(hessian_eigenvalues[0, :, :], cmap=cmap)
        ax[1].imshow(hessian_eigenvalues[1, :, :], cmap=cmap)
    else:
        _, ax = plt.subplots(1, 3, figsize=(8, 4))
        ax[0].imshow(hessian_eigenvalues[0, :, :], cmap=cmap)
        ax[1].imshow(hessian_eigenvalues[1, :, :], cmap=cmap)
        ax[2].imshow(hessian_eigenvalues[2, :, :], cmap=cmap)
    plt.show()


def hm_jerman(image, sigmas=range(1, 10, 2), tau=0.5, black_ridges=False,
              do_tophat=False, tophat_size=None,
              mode='reflect', cval=0, verbose=False):
    """
    Filter an image with the Jerman vesselness filter [1]. Available for MATLAB here [2].
    Related to the popular Frangi vesselness approach [3].
    This filter is based on the analysis of the eigenvalues of the Hessian matrix of an image and
    can be used to detect continuous ridges, e.g. vessels, wrinkles, rivers.
    Defined only for 2-D and 3-D images.

    Parameters
    ----------
    image : (N, M[, P]) ndarray
        Array with input image data [2D or 3D].
        This image will be normalized to [0..1].
    sigmas : iterable of floats, optional
        Sigmas used as scales of filter, i.e.,
        np.arange(scale_range[0], scale_range[1], scale_step)
    tau : float [0.5..1.0], optional
        Parameter used for the regularization.
        Lower values result in increased response (also for noise)
    black_ridges : boolean, optional
        When True, the filter detects black ridges;
        When False, it detects white ridges (default).
    do_tophat: boolean, optional
        When True, a top-hat transformation is performed as a pre-processing step to isolate the vasculature.
    tophat_size: integer, optional
        Determines the size of the structured element used in the top-hat transformation.
        Features larger than this structured element are filtered out
        Thus, set the size larger than the highest sigma used.
        If not specified, an tophat_size is computed automatically based on the inputed sigmas
    mode : {'constant', 'reflect' (default), 'wrap', 'nearest', 'mirror'}, optional
        How to handle values outside the image borders.
        The use of 'reflect' aka default is recommended
    cval : float, optional
        Used in conjunction with mode 'constant', the value outside
        the image boundaries.
    verbose : bool, optional
        If true the Hessian eigenvalues are plotted per scale.
    Returns
    -------
    filtered_image: (N, M[, P]) ndarray
        Filtered image (maximum of pixels across all scales).
    max_response_sigma: (N, M[, P]) ndarray
        Maximal response/sigma for each pixel across all scales.

    Notes
    -----
    Proposed by T. Jerman et al in [1]_
    MATLAB implementation in [2]_
    Re-written for Python by Hendrik Mattern, August 2020
    The code relies heavily on the scikit-image, and also the code structure is largely following the scikit-image
    Frangi implementation (very similar up to the response function, which is re-implemented from Jerman et al.)

    References
    ----------
    .. [1] T. Jerman, F. Pernus, B. Likar, Z. Spiclin,
        "Enhancement of Vascular Structures in 3D and 2D Angiographic Images",
        IEEE Transactions on Medical Imaging, 35(9), p. 2107-2118 (2016), doi=10.1109/TMI.2016.2550102
    .. [2] T. Jerman: https://github.com/timjerman/JermanEnhancementFilter
    .. [3] Frangi, A. F., Niessen, W. J., Vincken, K. L., & Viergever, M. A.
        (1998,). Multiscale vessel enhancement filtering. In International
        Conference on Medical Image Computing and Computer-Assisted
        Intervention (pp. 130-137). Springer Berlin Heidelberg.
        :DOI:`10.1007/BFb0056195`
    """

    # Check image dimensions (could also be done with private method <check_nD(image, [2, 3])> )
    if not len(image.shape) in [2, 3]:
        raise ValueError('image is not 2D or 3D')

    # Check sigma scales (could also be done with private method <sigmas = _check_sigmas(sigmas)> )
    if np.any(np.asarray(sigmas).ravel() < 0.0):
        raise ValueError('Sigma values should be non-negative.')

    # Normalize input image
    image = (image - np.min(image)) / (np.max(image) - np.min(image))

    # Top-hat transformation (optional pre-processing:
    if do_tophat:
        # set default size of top-hat transformation if required:
        if tophat_size is None:
            tophat_size = np.int_(sigmas[-1] + 5.0)
        # create structure element to perform the transformation with
        if len(image.shape) == 2:
            structure_element = disk(tophat_size)
        else:
            structure_element = ball(tophat_size)
        # do black or white top-hat transformation depending on the rigid-flag
        # FYI: after the top-hat transformation the output is hyperintense, aka white vessels
        # Hence, adapt black_ridges flag if required
        if black_ridges:
            image = black_tophat(image, selem=structure_element)
            black_ridges = False
        else:
            image = white_tophat(image, selem=structure_element)

    # Initialize filtered image
    filtered_image = np.zeros(image.shape)

    # Initialize array which stores scale of the maximum response (can be used to approx. vessel diameter)
    max_response_sigma = np.zeros(image.shape)

    # Filtering for all (sigma) scales
    for sigma in sigmas:
        print('current sigma: ', str(sigma))
        # Compute the eigenvalues of the Hessian (ie. lambda 1, 2 (and 3))
        #   1. Compute the Hessian of the image (use xy instead of rc order)
        #   2. Correct for scale
        #   3. Get eigenvalues
        #   4. Sort by absolute value
        hessian_eigenvalues = compute_hessian_eigenvalues(image=image, sigma=sigma, sorting='abs', mode=mode, cval=cval)

        if verbose:
            _hm_plot_hessian_eigenvalues(hessian_eigenvalues)

        # Compute vesselness response function
        response = _hm_jerman_vesselness_response(hessian_eigenvalues, black_ridges=black_ridges, tau=tau)

        # enhanced output is the maximum across all scales
        filtered_image = np.maximum(response, filtered_image)

        # get scale of maximum response for diameter approximation
        #   (if current scale is current maximum, replace the sigma entry in max_scale_per_voxel
        max_response_sigma[filtered_image == response] = sigma

    # Return for every pixel the maximum value over all (sigma) scales
    return filtered_image, max_response_sigma


def _hm_pruning(data, pruning_cut_off=0):
    if pruning_cut_off > 0:
        remove_small_objects(data, pruning_cut_off, connectivity=len(data.shape), in_place=True)
    return data


def hm_filtered2skeleton(image_filtered, pruning_cut_off=0, verbose_plot=False):
    # 0. Init verbose plotting
    if verbose_plot:
        n_col = 2
        n_row = 2
        _, axs = plt.subplots(ncols=n_col, nrows=n_row, sharey=True)
        axs = axs.ravel()
        _hm_plotting_processing(axs, n_col*n_row, 0, image_filtered, 'unprocessed vesselness image')

    # 1. Threshold from multi-otsu
    # below first threshold is background, above last threshold are (large/bright) vessels
    # small vessels lie in between thresholds
    # FYI: consider only non-zero values
    thresholds = threshold_multiotsu(image_filtered[image_filtered != 0].ravel(), 3)

    if verbose_plot:
        _hm_plotting_processing(axs, n_col*n_row, 1, np.digitize(image_filtered, bins=thresholds),
                                'multi-otus-threshold')

    # 2. Hysteresis thresholding based on multi-Otsu threshold of segmentation
    # this helps to find small vessels (in between thresholds)
    # which are connected to large/bright vessels (above second threshold)
    image_bin = apply_hysteresis_threshold(image_filtered, thresholds[0], thresholds[1])

    # 3. Pruning
    image_bin = _hm_pruning(image_bin, pruning_cut_off)

    if verbose_plot:
        _hm_plotting_processing(axs, n_col*n_row, 2, image_bin, 'binary image after hysteresis thresholding')

    # 4. Skeletonize
    # method='lee' will be used automatically for 3D input
    image_skeleton = skeletonize(image_bin)

    if verbose_plot:
        _hm_plotting_processing(axs, n_col*n_row, 3, image_skeleton, 'skeleton')
        plt.show()

    return image_skeleton, image_bin


def hm_filtered2skeleton_iterative(image_filtered, n_threshold_iterations=10, kappa=0.1, pruning_cut_off=0,
                                   prevent_leaking=True, verbose=False):
    # Create a copy of the original filtered image which will be altered
    # Hence, by creating a copy (and not using the reference), the original filter output stays unaltered
    image_filtered_temp = image_filtered.copy()
    # Check parameters
    if n_threshold_iterations < 1:
        print('n_threshold_iterations < 1\n hard reset to 1')
        n_threshold_iterations = 1

    # Init verbose plotting
    if verbose:
        n_bins = 255
        n_col = 3
        n_row = 2
        _, axs = plt.subplots(ncols=n_col, nrows=n_row)
        axs = axs.ravel()
        _hm_plotting_processing(axs, n_col*n_row, 0, image_filtered_temp, 'unprocessed vesselness image')

    # Iterative thresholding approach
    for i in range(0, n_threshold_iterations):
        if verbose:
            print('iteration ', str(i+1), ' of ', str(n_threshold_iterations))

        # Threshold from multi-otsu
        # below first/lower threshold is background, above second/higher threshold are (large/bright) vessels
        # small vessels lie in between thresholds
        # FYI: consider only non-zero values
        thresholds = threshold_multiotsu(image_filtered_temp[image_filtered_temp != 0].ravel(), 3)

        if verbose:
            if i == 0:
                axs[1].hist(image_filtered_temp.ravel(), bins=n_bins)
                axs[1].set_yscale('log')
                axs[1].set_title('log-intensity histrogram (first iteration)')
                for thresh in thresholds:
                    axs[1].axvline(thresh, color='r')
            elif i == n_threshold_iterations-1:
                axs[2].hist(image_filtered_temp.ravel(), bins=n_bins)
                axs[2].set_yscale('log')
                axs[2].set_title('log-intensity histrogram (last iteration)')
                for thresh in thresholds:
                    axs[2].axvline(thresh, color='r')

        # hysteresis thresholding based on multi-Otsu thresholds
        # this helps to find small vessels (in between thresholds)
        # which are connected to large/bright vessels (above second threshold)
        image_bin = apply_hysteresis_threshold(image_filtered_temp, thresholds[0], thresholds[1])

        # prune the segmentation
        # everything below cut-off volume will be removed; helps cleaning up the segmentation
        image_bin = _hm_pruning(image_bin, pruning_cut_off)

        # Increase intensity of segmented voxels and repeat iterative thresholding
        # This is similar to a compressed sensing approach and should help to disentangle vessels from background
        image_filtered_temp += image_bin*kappa
        image_filtered_temp[image_filtered_temp > 1.0] = 1.0  # cut-off out-of-range values

    # Prevent leaking:
    # The iterative approach can produce a leaky segmentation (i.e. vessel diameter is over-estimated).
    # This is addressed by extracting the center-line of the iterative segmentation, applying the soft-thresholding
    # once to the initial filtered image only with the center-line and doing the multi-Otsu + hysteresis thresholding
    if prevent_leaking:
        # create new enhanced filtered image (center-line intensity == 1.0)
        image_filtered_temp = image_filtered + skeletonize(image_bin) * 1.0
        image_filtered_temp[image_filtered_temp > 1.0] = 1.0  # cut-off out-of-range values
        # apply thresholding
        thresholds = threshold_multiotsu(image_filtered_temp[image_filtered_temp != 0].ravel(), 3)
        image_bin = apply_hysteresis_threshold(image_filtered_temp, thresholds[0], thresholds[1])
        image_bin = _hm_pruning(image_bin, pruning_cut_off)

    if verbose:
        _hm_plotting_processing(axs, n_col*n_row, 3, image_bin, 'binary image after iterative thresholding')

    # Skeletonize
    # method='lee' will be used automatically for 3D input
    image_skeleton = skeletonize(image_bin)

    if verbose:
        _hm_plotting_processing(axs, n_col*n_row, 4, image_skeleton, 'skeleton')
        plt.show()
        axs[-1].axis('off')

    return image_skeleton, image_bin


def _hm_plotting_processing(axs, num, i, img, title):
    # handle 3D data as MIP
    if len(img.shape) == 3:
        img = np.max(img, axis=2)

    # handle case if only one axis is required
    cmap = plt.get_cmap('gray')
    if num == 1:
        axs.imshow(img, cmap=cmap)
        axs.set_title(title)
    else:
        axs[i].imshow(img, cmap=cmap)
        axs[i].set_title(title)
    return i+1


# todo:
def hm_extract_labels(segmentation, connectivity=None):
    if connectivity is None:
        connectivity = len(segmentation.shape)
# plt.imshow(label(segmentation, connectivity=len(image.shape))


def hm_overview_plot(image, image_filtered, image_bin, image_skeleton, response):
    # figure settings
    plt.rcParams.update({'font.size': 12,
                         'legend.fontsize': 14,
                         'figure.figsize': [12.0, 3.0]})

    # plot the response of the skeleton
    image_skeleton_response = image_skeleton * response

    # if 3D, plot maximum intensity projections
    if len(image.shape) == 3:
        image = np.max(image, axis=2)
        image_filtered = np.max(image_filtered, axis=2)
        image_bin = np.max(image_bin, axis=2)
        image_skeleton_response = np.max(image_skeleton_response, axis=2)

    cmap = plt.get_cmap('gray')
    fig, axs = plt.subplots(ncols=4)
    axs[0].imshow(image, cmap=cmap)
    axs[0].set_title('image')
    axs[1].imshow(image_filtered, cmap=cmap)
    axs[1].set_title('vesselness filter output')
    axs[2].imshow(image_bin, cmap=cmap)
    axs[2].set_title('binary segmentation')
    axs[3].imshow(image_skeleton_response)
    axs[3].set_title('skeleton with \nmax. response')
    for i in range(0, len(axs)):
        axs[i].set_axis_off()
    plt.show()

    # plt.savefig((title + '.jpg'),
    #             dpi=300,
    #             format='jpg',
    #             bbox_inches='tight',
    #             pad_inches=0)


def _hm_voxelwise_confusion_matrix(ground_truth, prediction):
    g = ground_truth.astype(dtype=bool)
    p = prediction.astype(dtype=bool)

    true_pos = np.logical_and(g, p)
    false_pos = np.logical_and(~g, p)
    false_neg = np.logical_and(g, ~p)
    true_neg = np.logical_and(~g, ~p)
    return true_pos, false_pos, false_neg, true_neg


def hm_confusion_matrix(ground_truth, prediction):
    true_pos, false_pos, false_neg, true_neg = _hm_voxelwise_confusion_matrix(ground_truth, prediction)
    return true_pos.sum(), false_pos.sum(), false_neg.sum(), true_neg.sum()


def hm_dice_coefficient(ground_truth, prediction):
    true_pos, false_pos, false_neg, _ = hm_confusion_matrix(ground_truth, prediction)
    return 2*true_pos / (2*true_pos + false_pos + false_neg)


def hm_segmentation_sensitivity(ground_truth, prediction):  # aka recall
    true_pos, _, false_neg, _ = hm_confusion_matrix(ground_truth, prediction)
    return true_pos / (true_pos + false_neg)


def hm_segmentation_specificity(ground_truth, prediction):
    _, false_pos, _, true_neg = hm_confusion_matrix(ground_truth, prediction)
    return true_neg / (true_neg + false_pos)


def hm_youden_index(sensitivity, specificity):
    return sensitivity + specificity - 1


def hm_create_mip(image, mip_start, mip_end, projection_axis):
    if projection_axis == 0:
        mip = np.max(image[mip_start:mip_end, :, :], axis=projection_axis)
    elif projection_axis == 1:
        mip = np.max(image[:, mip_start:mip_end, :], axis=projection_axis)
    else:
        mip = np.max(image[:, :, mip_start:mip_end], axis=projection_axis)
    return np.rot90(mip)


def hm_create_minip(image, mip_start, mip_end, projection_axis):
    if projection_axis == 0:
        mip = np.min(image[mip_start:mip_end, :, :], axis=projection_axis)
    elif projection_axis == 1:
        mip = np.min(image[:, mip_start:mip_end, :], axis=projection_axis)
    else:
        mip = np.min(image[:, :, mip_start:mip_end], axis=projection_axis)
    return np.rot90(mip)
